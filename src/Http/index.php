<?php

include '../../vendor/autoload.php';

use PedroPerafanCarrasco\StreamLoan\Http\Application;

$app = new Application();
$app->run();

echo $app->render();
