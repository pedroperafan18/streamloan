<?php
$this->layout('layout', ['title' => 'Score Data'])
?>

<div class="text-2xl text-green-900 my-3">
    Score Data
</div>

<div class="flex flex-col">
    <div class="w-full text-lg my-2">
        Users with score range between 20 and 50:
        <span class="font-bold">
            <?= $this->e($countScoreRange1) ?>
        </span>
    </div>

    <div class="w-full text-lg my-2">
        Users with score range between -40 and 0:
        <span class="font-bold">
            <?= $this->e($countScoreRange2) ?>
        </span>
    </div>

    <div class="w-full text-lg my-2">
        Users with score range between 0 and 80:
        <span class="font-bold">
            <?= $this->e($countScoreRange3) ?>
        </span>
    </div>

    <div class="w-full text-lg my-2">
        Users from CA, woman, without legal age and negative score:
        <span class="font-bold">
            <?= $this->e($countCondition1) ?>
        </span>
    </div>

    <div class="w-full text-lg my-2">
        Users from CA, woman, without legal age and positive score:
        <span class="font-bold">
            <?= $this->e($countCondition2) ?>
        </span>
    </div>

    <div class="w-full text-lg my-2">
        Users from CA, woman, with legal age and positive score:
        <span class="font-bold">
            <?= $this->e($countCondition3) ?>
        </span>
    </div>
</div>


<div class="font-bold text-lg mt-10">
    Thank you for your time!
</div>
