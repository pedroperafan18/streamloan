<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title><?=$this->e($title)?></title>
</head>
<body>
    <nav class="bg-green-700 text-white py-3">
        <div class="container mx-auto flex justify-between items-center">
            <a href="https://www.streamloan.io/" class="text-2xl" target="_blank">
                StreamLoan
            </a>
            <div class="flex text-sm items-center">
                <span class="font-bold">
                    Coding Test
                </span>
                <a href="https://github.com/Perafan18" class="italic hover:text-gray-100 ml-2" target="_blank">
                    (By Pedro Perafán Carrasco)
                </a>
            </div>
        </div>
    </nav>
    <div class="container mx-auto">
        <?=$this->section('content')?>
    </div>
</body>
</html>
