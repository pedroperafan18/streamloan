<?php

namespace PedroPerafanCarrasco\StreamLoan\Http;

use League\Plates\Engine;
use PedroPerafanCarrasco\StreamLoan\Executable;

class Application  extends \PedroPerafanCarrasco\StreamLoan\Application implements Executable
{
    private Engine $views;

    /**
     * @return void
     */

    public function run(): void
    {
        $this->views = new Engine('views');
    }

    /**
     * @return string
     */

    public function render(): string
    {
        return $this->views->render('home', [
            'countScoreRange1' => $this->scoreData->getCountOfUsersWithinScoreRange(20, 50),
            'countScoreRange2' => $this->scoreData->getCountOfUsersWithinScoreRange(-40, 0),
            'countScoreRange3' => $this->scoreData->getCountOfUsersWithinScoreRange(0, 80),
            'countCondition1' => $this->scoreData->getCountOfUsersByCondition('CA', 'w', false, false),
            'countCondition2' => $this->scoreData->getCountOfUsersByCondition('CA', 'w', false, true),
            'countCondition3' => $this->scoreData->getCountOfUsersByCondition('CA', 'w', true, true)
        ]);
    }
}
