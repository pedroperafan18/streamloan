<?php
namespace PedroPerafanCarrasco\StreamLoan;

use League\Config\Configuration;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\Reader;
use Nette\Schema\Expect;

abstract class Application
{
    private const DEFAULT_DIRECTORY = 'data';
    protected Configuration $config;
    protected ScoreDataIndexer $scoreData;

    public function __construct()
    {
        $this->schemaConfig();
        $this->loadConfig();
        $this->scoreData = new ScoreDataIndexer($this->file());
    }

    /**
     * @return void
     */

    private function schemaConfig(): void
    {
        $this->config = new Configuration([
            'dataset' => Expect::structure([
                'directory' => Expect::string()->default(self::DEFAULT_DIRECTORY),
                'filename' => Expect::string(),
                'delimiter' => Expect::string(),
                'header_offset' => Expect::int(),
            ])
        ]);
    }

    /**
     * @return void
     */

    private function loadConfig(): void
    {
        $this->config->merge(require(dirname(__FILE__).'/../config/app.php'));
    }


    /**
     * @return Reader
     */

    private function file(): Reader
    {
        try {
            $csv = Reader::createFromPath($this->pathFile());
            $csv->setDelimiter($this->config->get('dataset.delimiter'));
            $csv->setHeaderOffset($this->config->get('dataset.header_offset'));

            return $csv;
        } catch (InvalidArgument $exception) {
            die('Invalid argument. Check your dataset config.');
        } catch (Exception $exception) {
            die('File not found:' . $this->pathFile());
        }
    }

    /**
     * @return string
     */

    private function pathFile(): string
    {
        return
            __DIR__ .
            '/../' .
            $this->config->get('dataset.directory') .
            '/' .
            $this->config->get('dataset.filename');
    }
}
