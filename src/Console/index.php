<?php

include 'vendor/autoload.php';

use PedroPerafanCarrasco\StreamLoan\Console\Application;

$app = new Application();
$app->run();
