<?php

namespace PedroPerafanCarrasco\StreamLoan\Console;

use League\CLImate\CLImate;
use PedroPerafanCarrasco\StreamLoan\Executable;

class Application extends \PedroPerafanCarrasco\StreamLoan\Application implements Executable
{
    private CLImate $cli;

    public function __construct()
    {
        parent::__construct();
        $this->cli = new CLImate;
    }

    /**
     * @return void
     */

    public function run(): void
    {
        $this->cli->out('Users with score range between 20 and 50:');
        $this->cli->out($this->scoreData->getCountOfUsersWithinScoreRange(20, 50));

        $this->cli->out('Users with score range between -40 and 0:');
        $this->cli->out($this->scoreData->getCountOfUsersWithinScoreRange(-40, 0));

        $this->cli->out('Users with score range between 0 and 80:');
        $this->cli->out($this->scoreData->getCountOfUsersWithinScoreRange(0, 80));

        $this->cli->out('Users from CA, woman, without legal age and negative score:');
        $this->cli->out($this->scoreData->getCountOfUsersByCondition('CA', 'w', false, false));

        $this->cli->out('Users from CA, woman, without legal age and positive score:');
        $this->cli->out($this->scoreData->getCountOfUsersByCondition('CA', 'w', false, true));

        $this->cli->out('Users from CA, woman, with legal age and positive score:');
        $this->cli->out($this->scoreData->getCountOfUsersByCondition('CA', 'w', true, true));

        $this->cli->out('Done!');
    }
}
