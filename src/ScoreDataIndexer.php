<?php

namespace PedroPerafanCarrasco\StreamLoan;

use League\Csv\Reader;

class ScoreDataIndexer implements ScoreDataIndexerInterface
{
    private const LEGAL_AGE = 21;
    private Reader $csv;
    private array $records;

    /**
     * @param Reader $csv
     */

    public function __construct(Reader $csv)
    {
        $this->csv = $csv;
        $this->records = $this->getRecords();
    }

    /**
     * @inheritDoc
     */
    public function getCountOfUsersWithinScoreRange(int $rangeStart, int $rangeEnd): int
    {
        return count(array_filter($this->records, function($record) use ($rangeStart, $rangeEnd) {
            return $record['Score'] >= $rangeStart && $record['Score'] <= $rangeEnd;
        }));
    }

    /**
     * @inheritDoc
     */

    public function getCountOfUsersByCondition(string $region, string $gender, bool $hasLegalAge, bool $hasPositiveScore): int
    {
        return count(array_filter($this->records, function($record) use ($region, $gender, $hasLegalAge, $hasPositiveScore) {
            return $record['Region'] == $region &&
                $record['Gender'] == $gender &&
                ($record['Age'] >= self::LEGAL_AGE) == $hasLegalAge &&
                ($record['Score'] > 0) == $hasPositiveScore;
        }));
    }

    /**
     * @return array
     */

    private function getRecords(): array
    {
        return $this->csv->jsonSerialize();
    }
}
