<?php

namespace PedroPerafanCarrasco\StreamLoan;

interface Executable
{
    /**
     * @return void
     */

    public function run(): void;
}
