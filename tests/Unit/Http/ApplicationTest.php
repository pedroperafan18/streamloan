<?php

namespace Unit\Http;

use PedroPerafanCarrasco\StreamLoan\Executable;
use PedroPerafanCarrasco\StreamLoan\Http\Application;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->klass = new Application();
    }

    public function testImplementsExecutable(): void
    {
        $this->assertInstanceOf(Executable::class, $this->klass);
    }
}
