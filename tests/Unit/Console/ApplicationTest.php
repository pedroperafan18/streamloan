<?php

namespace Unit\Console;

use PedroPerafanCarrasco\StreamLoan\Console\Application;
use PedroPerafanCarrasco\StreamLoan\Executable;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->klass = new Application();
    }

    public function testImplementsExecutable(): void
    {
        $this->assertInstanceOf(Executable::class, $this->klass);
    }
}
