<?php

return [
    'dataset' => [
        /*
         * Here you can change de directory of the data (Optional)
         */

        // 'directory' => 'data',

        /*
         * Here you can change de filename of the data
         */

        'filename' => 'dataset.csv',


        /*
         * Change delimiter character.
         * https://csv.thephpleague.com/9.0/connections/controls/
         */

        'delimiter' => ';',


        /*
         * Set header row.
         * https://csv.thephpleague.com/9.0/reader/
         */
        'header_offset' => 0,
    ]
];
