# Coding Test

Hello Stream Loan Team!

## PHP

You can check the results on the browser or console.

### Setup

Please consider the follow versions

| PHP      | 8.1 |
|----------|-----|
| Composer | 2.1 |

Install dependencies

```
composer install
```

### Web

```
php -S localhost:8000 -t src/Http
```

Now got to the browser and open [localhost:8000](localhost:8000)

### Console

```
php src/Console/index.php
```


### Testing

```
./vendor/bin/phpunit
```

## Database

The SQL statement is:

```sql
SELECT branch.country, branch.state, avg(value) 
FROM loan
INNER JOIN branch ON loan.branch_id = branch.id
WHERE loan.is_active=1
GROUP BY branch.country, branch.state;
```
